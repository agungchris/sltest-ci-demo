function runVerification()

%% Git Settings

% gitPrivateToken = 'sA8kyW_Du4SDQY89jUSH';asdasdas
% [~, commit] = system('git rev-parse --short HEAD');
% [~, branch] = system('git rev-parse --abbrev-ref HEAD');
% 
% gitLabAPI = 'https://insidelabs-git.mathworks.com/api/v4/';
% ProjectID = '10201'; %Jenkins,
% writeOptions = weboptions('MediaType','application/x-www-form-urlencoded',...
%     'HeaderFields', ...
%     {'PRIVATE-TOKEN',gitPrivateToken});

try 
    [results,failed] = runMatlabTests(true,true,true,false,true);
    
    if failed
        % get and export results
        resultsLocation = fullfile(pwd,'results');
        if exist(resultsLocation,'dir') ~= 7
            mkdir(resultsLocation);
        end
        
        sltest.testmanager.exportResults(results(end),fullfile(resultsLocation,'failureResults.mldatx'));
%         writeIssueGitLab(gitLabAPI,ProjectID,commit,branch,writeOptions);
    else
%         writeMergeGitLab(gitLabAPI,ProjectID,commit,branch,writeOptions);
    end
    
catch e
    disp(getReport(e,'extended'));
end

exit(failed);



