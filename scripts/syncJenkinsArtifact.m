function syncJenkinsArtifact()
% COPYJENKINSSLXCFILES copies slxc files from Jenkins lastSuccessfulBuild
%   archive location to user's cache folder.
%
%   buildLocation: the Jenkins build location

% build archive path
JenkinsPathReport = fullfile('C:\Program Files (x86)', 'Jenkins', 'workspace', 'sltest-ci','TestReport.pdf');
JenkinsPathResult = fullfile('C:\Program Files (x86)', 'Jenkins', 'workspace', 'sltest-ci','results','failureResults.mldatx');

% get cache folder location
currentPathReport = fullfile(pwd);
currentPathResult = fullfile(pwd,'results');


if exist(JenkinsPathReport)==2
    copyfile(JenkinsPathReport, currentPathReport);
end


if exist(JenkinsPathResult)==2
    copyfile(JenkinsPathResult, currentPathResult);
    open(fullfile(currentPathResult,'failureResults.mldatx'))
elseif exist(JenkinsPathResult)==0
    currentPathResult = fullfile(pwd,'results','failureResults.mldatx');
    if currentPathResult == 2
    delete(currentPathResult)
    end
end
    
end
