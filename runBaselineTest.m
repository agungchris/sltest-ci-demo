function runBaselineTest()
totalFailures = 0;
% gitPrivateToken = 'sA8kyW_Du4SDQY89jUSH';
%
% sim('basemodel.slx')
try
    
%     [~, commit] = system('git rev-parse --short HEAD');
%     [~, branch] = system('git rev-parse --abbrev-ref HEAD');
%     
%     gitLabAPI = 'https://insidelabs-git.mathworks.com/api/v4/';
%     ProjectID = '10201'; %Jenkins,
%     writeOptions = weboptions('MediaType','application/x-www-form-urlencoded',...
%         'HeaderFields', ...
%         {'PRIVATE-TOKEN',gitPrivateToken});
    
    result = runtests(fullfile(pwd,'test')); % run the test manager (the only one in the project)
    display(result);
    totalFailures = sum(vertcat(result(:).Failed));
    result = sltest.testmanager.getResultSets;
    
    
    if totalFailures == 0
        % If no failures then create a merge request
%         writeMergeGitLab(gitLabAPI,ProjectID,commit,branch,writeOptions);
        
    else
        % get and export results
        resultsLocation = fullfile(pwd,'results');
        if exist(resultsLocation,'dir') ~= 7
            mkdir(resultsLocation);
        end
        
        sltest.testmanager.exportResults(result,fullfile(resultsLocation,'failureResults.mldatx'));
%         writeIssueGitLab(gitLabAPI,ProjectID,commit,branch,writeOptions);
        
    end
    
catch e
    disp(getReport(e,'extended'));
end

% try
%     failed = runMatlabTests(true,true,true);
% catch e
%     disp(getReport(e,'extended'));
% end
% 
% exit(totalFailures>0 | failed);


end

